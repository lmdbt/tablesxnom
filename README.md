# Tables de Multiplication pour Élèves

## Description
Ce projet est un document $\LaTeX$ permettant de générer des fiches de révision sur les tables de multiplication pour des élèves. Chaque fiche est personnalisée avec le nom de l'élève et présente les tables de 1 à 10.

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_11df59c16967339c362abde46f59ca21.png)

## Installation

1. Clonez ce dépôt sur votre machine locale.
2. Assurez-vous d'avoir $\LaTeX$ installé sur votre ordinateur.

## Utilisation

1. Placez un fichier `eleves.csv` dans le même dossier que le document $\LaTeX$. Le fichier CSV doit avoir la structure suivante :

    ```
    Eleve
    Prénom1 Nom1
    Prénom2 Nom2
    ...
    ```

2. Exécutez le fichier $\LaTeX$ pour générer le PDF des fiches.
3. Imprimez et plastifiez les fiches pour une utilisation à long terme.

## Objectif

Ces fiches visent à aider les élèves à apprendre et à réviser leurs tables de multiplication de manière efficace. Elles peuvent être imprimées et plastifiées pour augmenter l'engagement des élèves.

## Licence
Ce projet est disponible sous licence CC-BY. Vous êtes libre de le copier, de le modifier et de le redistribuer.

