# Utiliser le Générateur de Tables de Multiplication Nomminative sur $\LaTeX$

## À propos

Ce document LaTeX est conçu pour générer des fiches de tables de multiplication personnalisées pour chaque élève listé dans le document. Chaque fiche affiche les tables de multiplication de 1 à 10, personnalisées avec le nom de l'élève.

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_11df59c16967339c362abde46f59ca21.png)

## Comment l'utiliser

### Vous maitrisez $\LaTeX$

Vous n'avez plus qu'à télécharger le code [ici en noir et blanc](https://forge.apps.education.fr/lmdbt/tablesxnom/-/blob/main/tables_multiplication_nominative.tex) ou [ici en couleur](https://forge.apps.education.fr/lmdbt/tablesxnom/-/blob/main/tables_multiplication_nominative_couleurs.tex) et vous placez dans le même dossier votre liste d'élèves en suivant ce modèle CSV [ici](https://forge.apps.education.fr/lmdbt/tablesxnom/-/blob/main/eleves.csv). Vous compilez et c'est fini !

### Vous ne maitrisez pas $\LaTeX$ et mais ce n'est pas grave. Voici un petit tuto pour vous !

Même si vous n'avez pas d'expérience avec LaTeX, vous pouvez utiliser et modifier ce document en suivant les instructions ci-dessous :

<p id="Tuto1"></p>
1. **Modifier la liste des élèves :**
   - **Accédez** à la page web contenant le code LaTeX (en cliquant [ici](#Le_code)).
   - Dans le code, **repérez** le bloc commençant par `\begin{filecontents*}{eleves.csv}` et terminant par `\end{filecontents*}`.
   - **Remplacez** les noms existants (par exemple, Alice, Bob, Charlie) par les noms des élèves pour lesquels vous souhaitez créer des fiches. Vous pouvez ajoutez les noms de famille en les séparant d'un espace du prénom.
   - Assurez-vous que chaque nom est sur une nouvelle ligne sans espaces supplémentaires.

<p id="Tuto2"></p>
2. **Compiler le document :**
   - **Copiez** tout le code modifié.
   - **Allez** sur le compilateur LaTeX présent sur cette page en cliquant [ici](#Le_compil).
   - **Créez** un nouveau projet (donnez un nom de projet et cliquez sur `Créer`).
      ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_4f2950dc7e7f7744280a2f0ef54ea661.png)
   - **Collez** le code dans l'éditeur du projet.
   - **Compilez** le document en utilisant le bouton vert de compilation.
   ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_0e371a5b3a10b0248a272119cad91586.png)
   - Une fois la compilation terminée, le **PDF généré** vous n'avez plus qu'à le **télécharger et imprimer** !
   ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_a86accdc517ddb8e9672543658d213b7.png)

## Conseils

- Si des erreurs surviennent lors de la compilation, assurez-vous que le formatage des noms est correct et que le reste du code n'a pas été modifié accidentellement.
- Ce document peut être personnalisé davantage en ajoutant ou modifiant des packages LaTeX ou en ajustant le style des fiches. Pour cela, une connaissance de base de LaTeX est recommandée.

<p id="Le_code"></p>
## Le code

Vous pouvez revenir au tutoriel à tout moment en cliquant : [ici](#Tuto1)

<textarea class="language-latex" style="width: 90%; display: block; margin: 0 auto; height: 400px; font-size: 18px; color: blue;">
\documentclass[french,12pt]{article}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage[a4paper, landscape]{geometry}
\usepackage{forloop}
\usepackage{pgffor}
\usepackage{csvsimple}
\usepackage{frcursive}
\usepackage{xcolor}
\usepackage{colortbl}

\newcounter{ct}
\newcounter{ct2}

\definecolor{table1}{RGB}{204, 242, 255} % blue!15
\definecolor{table2}{RGB}{255, 234, 204} % orange!15
\definecolor{table3}{RGB}{255, 255, 230} % yellow!15
\definecolor{table4}{RGB}{242, 255, 230} % green!15
\definecolor{table5}{RGB}{250, 230, 255} % pink!15
\definecolor{table6}{RGB}{230, 230, 230} % gray!15
\definecolor{table7}{RGB}{255, 204, 204} % red!15
\definecolor{table8}{RGB}{230, 230, 255} % purple!15
\definecolor{table9}{RGB}{204, 255, 204} % lightgreen!15
\definecolor{table10}{RGB}{230, 204, 255} % lightpurple!15

\pagestyle{empty}

\geometry{%
	a4paper, landscape,
	body={280mm,200mm},
	left=8mm, top=7mm,
	headheight=9mm, headsep=3mm}

\renewcommand{\rmdefault}{\sfdefault}
\setlength{\parindent}{0pt}

\begin{document}
	
	\csvreader[head to column names]{eleves.csv}{}{%
		\begin{center}
			\LARGE Mes tables de multiplication \\ \textcursive{\Eleve}
		\end{center}
		\begin{minipage}{1\textwidth}
			\Large
			\begin{center}
				\forloop{ct}{1}{\value{ct} < 6}{
					\begin{tabular}{|c|}
						\hline
						\cellcolor{table\arabic{ct}}Table de \arabic{ct} \\ \hline
						\forloop{ct2}{1}{\value{ct2} < 11}{
							\cellcolor{table\arabic{ct}}
							\ifnum\value{ct2}<10
							$\arabic{ct} \times \arabic{ct2} = \the\numexpr\value{ct} * \value{ct2}\relax$ \\
							\arrayrulecolor{black}
							\else
							$\arabic{ct} \times \arabic{ct2} = \the\numexpr\value{ct} * \value{ct2}\relax$
							\fi
						} \\
						\arrayrulecolor{black}\hline
					\end{tabular}
					\quad
				}
			\end{center}
		\end{minipage}
		\medskip
		
		\begin{minipage}{1\textwidth}
			\Large
			\begin{center}
				\forloop{ct}{6}{\value{ct} < 11}{
					\begin{tabular}{|c|}
						\hline
						\cellcolor{table\arabic{ct}}Table de \arabic{ct} \\ \hline
						\forloop{ct2}{1}{\value{ct2} < 11}{
							\cellcolor{table\arabic{ct}}
							\ifnum\value{ct2}<10
							$\arabic{ct} \times \arabic{ct2} = \the\numexpr\value{ct} * \value{ct2}\relax$ \\
							\arrayrulecolor{black}
							\else
							$\arabic{ct} \times \arabic{ct2} = \the\numexpr\value{ct} * \value{ct2}\relax$
							\fi
						} \\
						\arrayrulecolor{black}\hline
					\end{tabular}
					\quad
				}
			\end{center}
		\end{minipage}
		\newpage
	}
	
\end{document}
</textarea>


<p id="Le_compil"></p>
## Le compilateur 

Vous pouvez revenir au tutoriel à tout moment en cliquant : [ici](#Tuto2)

<iframe src="https://www.edutex.ensciences.fr/" width="90%" height="600px" style="border:none; display: block; margin: 0 auto;"></iframe>

